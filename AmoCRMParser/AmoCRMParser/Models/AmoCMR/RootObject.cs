﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmoCRMParser.Models.AmoCMR;

namespace AmoCRMParser.Models.AmoCRM {
	public class RootObject {
		public Leads leads { get; set; }
		public  Account account { get; set; }

		public override string ToString() {
			return leads.ToString();
		}
	}
}
