﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmoCRMParser.Models.AmoCMR {
	public class Account {
		public string subdomain { get; set; }
		public int id { get; set; }

		public Links _links { get; set; }

	}
}
