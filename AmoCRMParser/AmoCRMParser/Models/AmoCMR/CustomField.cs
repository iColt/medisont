﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmoCRMParser.Models.AmoCRM {
	public class CustomField {
		public string id { get; set; }
		public string name { get; set; }
		public List<string> values { get; set; }
	}
}
