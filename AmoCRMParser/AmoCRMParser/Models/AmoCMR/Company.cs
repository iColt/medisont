﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmoCRMParser.Models.AmoCMR {
	public class Company {
		public int id { get; set; }

		public string name { get; set; }
	}
}
