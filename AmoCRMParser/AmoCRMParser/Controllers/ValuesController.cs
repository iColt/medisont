using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AmoCRMParser.AmoCRM;
using AmoCRMParser.Models.AmoCMR;
using AmoCRMParser.Models.AmoCRM;
using Manatee.Trello;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AmoCRMParser.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

		// POST api/values
		//      [HttpPost]
		//public void Post([FromForm] RootObject value)
		//      {
		//       //var ContentType = HttpContext.Request.ContentType;
		//       //var data = HttpContext.Request.Form;
		//       //var n = Newtonsoft.Json.JsonConvert.SerializeObject(data);
		//	//var con = HttpContext.Request.Form.

		//	//string s = " ";
		//	//foreach (var key in data)
		//	//{
		//	//	s = s + " " + key;
		//	//}
		//	var trelloAuth = new TrelloAuthorization();
		//       trelloAuth.AppKey = "108061f4d9e740493c55039c71b2cb85";
		//       trelloAuth.UserToken = "432b0dfaeceb5051e7be33a33a6fec196bab8756b0c67839fdedcdb7445d5e68";
		//       string boardId = "5c7eb5d7a58fe46d8e9b9fd6";
		//       string listId = "5c7eb5e1aafe553d78e1ca26";
		//       ITrelloFactory factory = new TrelloFactory();
		//       var board = factory.Board(boardId, trelloAuth);
		//       board.Refresh().Wait();
		//       var list = factory.List(listId, trelloAuth);
		//       list.Refresh().Wait();
		//       //var type = value.GetType().ToString();
		//       //var val = value.ToString();
		//	if(value == null) 
		//		list.Cards.Add("Name2", "kek", null, null, null, null, null).Wait();
		//	else
		//	{
		//		list.Cards.Add(value.leads.status.Length.ToString(), value.ToString(), null, null, null, null, null).Wait();
		//	}
		//      }


		[HttpPost]
		public ActionResult<string> Post([FromForm] RootObject value) {
			var trelloAuth = new TrelloAuthorization();
			trelloAuth.AppKey = "108061f4d9e740493c55039c71b2cb85";
			trelloAuth.UserToken = "432b0dfaeceb5051e7be33a33a6fec196bab8756b0c67839fdedcdb7445d5e68";
			//string boardId = "5c7eb5d7a58fe46d8e9b9fd6";
			string listId = "5c7eb5e1aafe553d78e1ca26";
			ITrelloFactory factory = new TrelloFactory();
			//var board = factory.Board(boardId, trelloAuth);
			//board.Refresh().Wait();
			var list = factory.List(listId, trelloAuth);
			list.Refresh().Wait();
			var authRes = Service.Auth("eduard.kobzik@mail.ru", "74ee770f74fd472cda4e7b2cd9463b46faa7d556", "https://eduardkobzik.amocrm.ru");
			//var contact = Service.GetContact("25673509", authRes.Cookie);
			var leads = Service.GetLeads(new LeadsRequest() { id = 1718585 }, authRes.Cookie);
			var lead = leads.First(x => x.id == value.leads.status.First()?.id);
			if (lead.status_id != 142)
				return "200";
			var contact = Service.GetContact(lead.contacts.id.First().ToString(), authRes.Cookie);
			//var company = Service.GetContact(lead.company_id.ToString(), authRes.Cookie);
			//using (var client = new WebClient()) {
			//	client.DownloadFile("https://drive.google.com/uc?export=download&id=15-KAfvVKhPDAkzbWzQtVfeEafgTt-nyj", "TrelloTemplate.txt");
			//}
			//string readText = System.IO.File.ReadAllText("TrelloCard.txt");
			string readContents = "##���� �����: {1} (������ � �������� � ������� ������)\r\n\r\n**����� ���: **\r\n\r\n------------------\r\n\r\n##��� ������� ����\r\n������ � ��: \r\n����������� �������: \r\n����������� �����: \r\n������� ����: \r\n��������: \r\nISBN: \r\n� ��� �������� �� ������ (���, ���, �����):\r\n\r\n------------------\r\n\r\n##��������� ������ (����������� � ��)\r\n\r\n------------------\r\n\r\n##1�\r\n\r\n------------------\r\n\r\n##������������\r\n\r\n------------------\r\n\r\n���������� ����: **{2}, {3}**\r\n**tel://{4}**\r\n�����: {5}\r\n���: {6}\r\n����� ��������: **{7}**";
			//using (StreamReader streamReader = new StreamReader("TrelloTemplate.txt", Encoding.UTF8)) {
			//	readContents = streamReader.ReadToEnd();
			//}

			var dueDate = lead.custom_fields.FirstOrDefault(x => x.name == "DueDate")?.customValues.First()?.value;
			var cardContent = String.Format(readContents, "", dueDate, contact.First().name, contact.First().custom_fields.First(x => x.name == "���������")?.customValues.First()?.value,
				contact.First()?.custom_fields.First(x => x.name == "�������")?.customValues.First()?.value, contact.First().custom_fields.First(x => x.name == "Email")?.customValues.First()?.value,
				contact.First().custom_fields.First(x => x.name == "��������")?.customValues.First()?.value, contact.First().custom_fields.First(x => x.name == "�����")?.customValues.First()?.value);

			var template = "{0}, {1}, ({2}) \n #������_-- #�����_--";
			var cardName = String.Format(template, contact.First()?.company.name, lead.name, contact.First()?.name);
			//Spoofi.AmoCrmIntegration.AmoCrmConfig conf = new AmoCrmConfig("Spoofi.AmoCrmIntegration", "eduardkobzik", "74ee770f74fd472cda4e7b2cd9463b46faa7d556");
			//Spoofi.AmoCrmIntegration.Service.AmoCrmService serv = new AmoCrmService(conf);
			//serv.GetItem<object>(new )
			//serv.
			list.Cards.Add(cardName, cardContent, null, null, null, null, null).Wait();

			return "200";
		}
		// PUT api/values/5
		[HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
