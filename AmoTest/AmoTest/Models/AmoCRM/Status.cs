﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmoTest.Models.AmoCRM {
	public class Status {
		public string id { get; set; }
		public string name { get; set; }
		public string old_status_id { get; set; }
		public string status_id { get; set; }
		public string price { get; set; }
		public string responsible_user_id { get; set; }
		public string last_modified { get; set; }
		public string modified_user_id { get; set; }
		public string created_user_id { get; set; }
		public string date_create { get; set; }
		public string account_id { get; set; }
		public List<CustomField> custom_fields { get; set; }

		public override string ToString() {
			return id + " " + name + " ";
		}
	}
}
