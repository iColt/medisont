﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmoTest.Models.AmoCRM {
	public class RootObject {
		public Leads leads { get; set; }

		public override string ToString() {
			return leads.ToString();
		}
	}
}
