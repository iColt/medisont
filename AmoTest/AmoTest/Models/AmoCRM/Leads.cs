﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmoTest.Models.AmoCRM {
	public class Leads {
		public Status status { get; set; }

		public override string ToString()
		{
			return status.ToString();
		}
	}
}
