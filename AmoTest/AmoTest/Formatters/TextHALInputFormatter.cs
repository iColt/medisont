﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace AmoTest.Formatters {
	public class TextHALInputFormatter : TextInputFormatter {
		public TextHALInputFormatter() {
			SupportedMediaTypes.Add("application/hal+json");
			SupportedEncodings.Add(UTF8EncodingWithoutBOM);
			SupportedEncodings.Add(UTF16EncodingLittleEndian);
		}

		protected override bool CanReadType(Type type) {
			return type == typeof(string);
		}

		public override async Task<InputFormatterResult> ReadRequestBodyAsync(
			InputFormatterContext context,
			Encoding encoding) {
			string data = null;
			using (var streamReader = context.ReaderFactory(
				context.HttpContext.Request.Body,
				encoding)) {
				data = await streamReader.ReadToEndAsync();
			}

			return InputFormatterResult.Success(data);
		}
	}
}