﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AmoTest.AmoCRM;
using AmoTest.Models;
using AmoTest.Models.AmoCRM;
using System.IO;
using System.Text;
using Manatee.Trello;
using Microsoft.AspNetCore.Mvc;
using Spoofi.AmoCrmIntegration;
using Spoofi.AmoCrmIntegration.Service;

namespace AmoTest.Controllers {
	[Route("api/[controller]")]
	[ApiController]
	public class ValuesController : ControllerBase {

		// GET api/values
		[HttpGet]
		public ActionResult<IEnumerable<string>> Get() {
			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		[HttpGet("{id}")]
		public ActionResult<string> Get(int id) {
			return "value";
		}

		// POST api/values
		[HttpPost]
		public ActionResult<string> Post(object value) {
			var trelloAuth = new TrelloAuthorization();
			trelloAuth.AppKey = "108061f4d9e740493c55039c71b2cb85";
			trelloAuth.UserToken = "432b0dfaeceb5051e7be33a33a6fec196bab8756b0c67839fdedcdb7445d5e68";
			//string boardId = "5c7eb5d7a58fe46d8e9b9fd6";
			string listId = "5c7eb5e1aafe553d78e1ca26";
			ITrelloFactory factory = new TrelloFactory();
			//var board = factory.Board(boardId, trelloAuth);
			//board.Refresh().Wait();
			var list = factory.List(listId, trelloAuth);
			list.Refresh().Wait();
			var authRes = Service.Auth("eduard.kobzik@mail.ru", "e74c9505467db7fe6090a224ae2b8ae1e599e7b0", "https://eduardkobzik.amocrm.ru");
			//var contact = Service.GetContact("25673509", authRes.Cookie);
			var leads = Service.GetLeads(new LeadsRequest() {id = 1718585 }, authRes.Cookie);
			var lead = leads.First(x => x.id == 2095011);
			var contact = Service.GetContact(lead.contacts.id.First().ToString(), authRes.Cookie);
			//var company = Service.GetContact(lead.company_id.ToString(), authRes.Cookie);
			using (var client = new WebClient()) {
				client.DownloadFile("https://drive.google.com/uc?export=download&id=15-KAfvVKhPDAkzbWzQtVfeEafgTt-nyj", "TrelloTemplate.txt");
			}
			//string readText = System.IO.File.ReadAllText("TrelloCard.txt");
			string readContents;
			using (StreamReader streamReader = new StreamReader("TrelloTemplate.txt", Encoding.UTF8)) {
				readContents = streamReader.ReadToEnd();
			}

			var dueDate = lead.custom_fields.FirstOrDefault(x => x.name == "DueDate")?.customValues.First()?.value;
			var cardContent = String.Format(readContents, "",  dueDate, contact.First().name, contact.First().custom_fields.First(x => x.name == "Должность")?.customValues.First()?.value,
				contact.First()?.custom_fields.First(x => x.name == "Телефон")?.customValues.First()?.value, contact.First().custom_fields.First(x => x.name == "Email")?.customValues.First()?.value,
				contact.First().custom_fields.First(x => x.name == "МгнСообщ")?.customValues.First()?.value, contact.First().custom_fields.First(x => x.name == "Адрес")?.customValues.First()?.value);

			var template = "{0}, {1}, ({2}) \n #Бумага_-- #Макет_--";
			var cardName = String.Format(template, contact.First()?.company.name, lead.name, contact.First()?.name);
			//Spoofi.AmoCrmIntegration.AmoCrmConfig conf = new AmoCrmConfig("Spoofi.AmoCrmIntegration", "eduardkobzik", "74ee770f74fd472cda4e7b2cd9463b46faa7d556");
			//Spoofi.AmoCrmIntegration.Service.AmoCrmService serv = new AmoCrmService(conf);
			//serv.GetItem<object>(new )
			//serv.
			list.Cards.Add(cardName, cardContent, null, null, null, null, null).Wait();

			return "200";
		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public void Put(int id, [FromBody] string value) {
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id) {
		}
	}
}
